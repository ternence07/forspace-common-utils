<!--
 * @description: 
 * @Author: T.W
 * @Github: https://github.com/TernenceWu0702
 * @Date: 2021-11-02 10:34:52
 * @LastEditors: T.W
 * @LastEditTime: 2021-11-02 11:50:56
-->


### 前端日常工具类

#### 数字相关工具

- 生成指定范围随机数

  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const num1 = CommonUtils.randomNum(1,100)
  
  console.log(num1)
  ```

  

- 数字千分位分隔

  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const str = CommonUtils.format(10100)
  
  console.log(str)
  ```

- 数字转化为大写金额

  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const str = CommonUtils.digitUppercase(10100)
  
  console.log(str)
  ```

- 数字转化为中文数字

  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const str = CommonUtils.intToChinese(10100)
  
  console.log(str)
  ```


#### 字符串相关工具

- 去掉字符左右空格

  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const str = CommonUtils.strTrimLeftOrRight(' adadad ')
  
  console.log(str)
  ```

- 全局替换某个字符为另一个字符
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const str = CommonUtils.strReplace('adadad','a','b')
  
  console.log(str)
  ```

- 将字母全部转化成以大写开头
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const str = CommonUtils.strToCapitalLetter('adadad')
  
  console.log(str)
  ```

- 生成随机字符串
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const str = CommonUtils.randomString(16)
  
  console.log(str)
  ```

- 字符串首字母大写
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const str = CommonUtils.fistLetterUpper('adadad')
  
  console.log(str)
  ```

- 手机号中间四位变成*
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const str = CommonUtils.telFormat('15645454554')
  
  console.log(str)
  ```

- 驼峰命名转换成短横线命名
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const str = CommonUtils.getKebabCase('commonUtils')
  
  console.log(str)
  ```

- 短横线命名转换成驼峰命名
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const str = CommonUtils.getKebabCase('common-utils')
  
  console.log(str)
  ```

- 全角转换为半角
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const str = CommonUtils.toCDB('啊哒哒，啊哒哒')
  
  console.log(str)
  ```

- 半角转换为全角
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const str = CommonUtils.toDBC('啊哒哒,啊哒哒')
  
  console.log(str)
  ```

#### 数组相关工具

- 判断是否是数组
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const flag = CommonUtils.arrJudge([1, 2, 3])
  
  console.log(flag)
  ```

- 数组去重
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const arr = CommonUtils.arrRemoveRepeat([1, 2, 3, 2])
  
  console.log(arr)
  ```

- 数组排序
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const arr = CommonUtils.arrOrderAscend([1, 2, 3, 2])
  
  console.log(arr)
  ```

- 数组最大值
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const num = CommonUtils.arrMax([1, 2, 3, 2])
  
  console.log(num)
  ```

- 数组求和
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const num = CommonUtils.arrSum([1, 2, 3, 2])
  
  console.log(num)
  ```

- 对象数组求和
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const objArr = [
      {a: 1, b: 2},
      {a: 1, b: 2},
      {a: 1, b: 2},
  ]
  
  const num = CommonUtils.arrObjSum(objArr)
  
  console.log(num)
  ```

- 合并数组，目前合并一维
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const arr = CommonUtils.arrConcat([1, 2, 3, 2], [1212, 121])
  
  console.log(arr)
  ```


- 判断是否包含某值
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const flag = CommonUtils.arrIncludeValue([1, 2, 3, 2], 3)
  
  console.log(flag)
  ```

- 数组并集，只支持一维数组
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const arr = CommonUtils.arrAndSet([1, 2, 3, 2], [1, 3, 4])
  
  console.log(arr)
  ```

- 数组交集，只支持一维数组
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const arr = CommonUtils.arrIntersection([1, 2, 3, 2], [1, 3, 4])
  
  console.log(arr)
  ```

- 数组差集，只支持一维数组
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'
  
  const arr = CommonUtils.arrDifference([1, 2, 3, 2], [1, 3, 4])
  
  console.log(arr)
  ```

- 对象数组根据对象某个属性分组
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const objArr = [
      {a: 1, b: 2},
      {a: 1, b: 2},
      {a: 2, b: 2},
  ]
  
  const arr = CommonUtils.ArrayGrouping(objArr, 'a')
  
  console.log(arr)
  ```

- 根据属性去重数组
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const objArr = [
      {a: 1, b: 2},
      {a: 1, b: 2},
      {a: 2, b: 2},
  ]
  
  const arr = CommonUtils.arrUniqueByAttr(objArr, 'a')
  
  console.log(arr)
  ```

- 找出数组中该属性最小值的一列
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const objArr = [
      {a: 1, b: 2},
      {a: 2, b: 2},
      {a: 3, b: 2},
  ]
  
  const arr = CommonUtils.arrMinByAttr(objArr, 'a')
  
  console.log(arr)
  ```

- 找出数组中该属性最大值的一列
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const objArr = [
      {a: 1, b: 2},
      {a: 2, b: 2},
      {a: 3, b: 2},
  ]
  
  const arr = CommonUtils.areMaxByAttr(objArr, 'a')
  
  console.log(arr)
  ```

- 扁平化数组 ==> 树形结构
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const objArr = [
      {a: 1, b: 0},
      {a: 2, b: 1},
      {a: 3, b: 2},
  ]
  
  const arr = CommonUtils.convert(objArr, {id: 'a', pId: 'b'})
  
  console.log(arr)
  ```

- 扁平化数组 ==> 树形结构
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const objArr = [
      {
          a: 1,
          b: 0,
          children: [
            {
              a: 2,
              b: 1,
            },
      ]},
  ]
  
  const arr = CommonUtils.convertFlat(objArr)
  
  console.log(arr)
  ```

- 数组乱序
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const objArr = [
      {a: 1, b: 2},
      {a: 1, b: 2},
      {a: 2, b: 2},
  ]
  
  const arr = CommonUtils.arrScrambling(objArr)
  
  console.log(arr)
  ```

- 数组扁平化
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const objArr = [
      {
          a: 1,
          b: 0,
          children: [
            {
              a: 2,
              b: 1,
            },
      ]},
  ]
  
  const arr = CommonUtils.flatten(objArr)
  
  console.log(arr)
  ```

- 数组中获取随机数
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const objArr = [
      {a: 1, b: 2},
      {a: 1, b: 2},
      {a: 2, b: 2},
  ]
  
  const arr = CommonUtils.sample(objArr)
  
  console.log(arr)
  ```

#### 数组相关工具

- 空对象判断
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const obj = {}
  
  const flag = CommonUtils.isEmptyObject(obj)
  
  console.log(flag)
  ```

- 判断两个对象是否相等
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const obj1 = {a: 1}
  const obj2 = {a: 2}

  const flag = CommonUtils.objIsEqual(obj1, obj2)
  
  console.log(flag)
  ```

- 深拷贝
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const obj1 = {a: 1}

  const obj = CommonUtils.deepClone(obj)
  
  console.log(obj)
  ```

#### 时间相关工具

- 获取年份
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const date = new Date()

  const str = CommonUtils.getYear(date)
  
  console.log(str)
  ```

- 获取当前月份
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const date = new Date()

  const str = CommonUtils.getMonth(date)
  
  console.log(str)
  ```

- 获取日
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const date = new Date()

  const str = CommonUtils.getDay(date)
  
  console.log(str)
  ```

- 获取星期几
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const date = new Date()

  const str = CommonUtils.getWhatDay(date)
  
  console.log(str)
  ```

- 获取当前月天数
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const num = CommonUtils.getMonthNum(2021, 11)
  
  console.log(num)
  ```

- 获取当前时间
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const date = new Date()

  const str = CommonUtils.getYyMmDdHhMmSs(date)
  
  console.log(str)
  ```

- 时间戳转化为年月日
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const str = CommonUtils.timesToYyMmDd(1635823152,  'yyyy-mm-dd', 'hh:mm:ss')
  
  console.log(str)
  ```

- 将年月日转化成时间戳
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const num = CommonUtils.YyMmDdToTimes('2021-11-02 11:19:12')
  
  console.log(num)
  ```

#### 浏览器存储相关工具

- 保存localStorage
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  CommonUtils.setLocalStorage('key', 'value', 24*60*60*1000)
  ```

-  获取localStorage
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const value = CommonUtils.getLocalStorage('key')
  
  console.log(value)
  ```

-  设置cookie
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const value = CommonUtils.setCookie('key', 'value', 24*60*60*1000)
  
  console.log(value)
  ```

-  读取cookie
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const value = CommonUtils.getCookie('key')
  
  console.log(value)
  ```

-  删除cookie
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  CommonUtils.delCookie('key')
  
  ```

#### JS相关工具

-  节流:当持续触发事件时，保证一定时间段内只调用一次事件处理函数
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const fun = () => {
      console.log(1)
  }

  const fun2 = CommonUtils.throttle(fun, 5000)

  fun2()
  fun2()
  ```

-  防抖:当持续触发事件时，一定时间段内没有再触发事件，事件处理函数才会执行一次
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const fun = () => {
      console.log(1)
  }

  const fun2 = CommonUtils.debounce(fun, 5000)

  fun2()
  fun2()
  ```

-  阻止冒泡事件
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  document.addEventListener('click',(e)=>{
    CommonUtils.stopPropagation(e)
  })
  ```

-  数据类型判断
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const str = CommonUtils.getType(121)

  console.log(str)
  ```

-  数据类型判断
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const obj = CommonUtils.deepClone({a: 1, b: new Date()})

  console.log(obj)
  ```

#### 事件相关工具

 ```typescript
  import CommonUtils from 'forspace-common-utils'

  const event = CommonUtils.EventEmitter()

  event.on('log', ()=>{
      console.log(1)
  })

  event.once('onceLog', ()=>{
      console.log(2)
  })

  event.emit('onceLog')
  event.emit('onceLog')

  event.emit('log')
  event.emit('log')

  event.off('onceLog',()=>{
      console.log(3)
  })
  event.allOff('log')
  ```

#### 终端相关工具

-  判断浏览器内核
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const str = CommonUtils.checkBrowser()

  console.log(str)
  ```

-  判断是终端类型,值有ios,android,iPad
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const str = CommonUtils.checkIosAndroidIPad()

  console.log(str)
  ```

-  判断是终端类型,值有ios,android,iPad
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const str = CommonUtils.checkIosAndroidIPad()

  console.log(str)
  ```

-  判断是否是微信,qq 或 uc
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const str = CommonUtils.checkWeixinQqUc()

  console.log(str)
  ```

-  判断是否是微信,qq 或 uc
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const str = CommonUtils.checkIsIPhoneX()

  console.log(str)
  ```

-  判断是移动还是PC设备
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const str = CommonUtils.isMobile()

  console.log(str)
  ```

-  判断是否是苹果
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const flag = CommonUtils.isAppleMobileDevice()

  console.log(flag)
  ```

-  判断是否是安卓移动设备
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const flag = CommonUtils.isAndroidMobileDevice()

  console.log(flag)
  ```

-  判断是Windows还是Mac系统
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const flag = CommonUtils.osType()

  console.log(flag)
  ```

-  浏览器型号和版本
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const flag = CommonUtils.getExplorerInfo()

  console.log(flag)
  ```

#### 浏览器相关工具

-  滚动到页面顶部
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  CommonUtils.scrollToTop()
  ```

-  滚动到页面底部
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  CommonUtils.scrollToBottom()
  ```

-  滚动到指定元素区域
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  CommonUtils.smoothScroll('title')
  ```

-  获取可视窗口高度
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const num = CommonUtils.getClientHeight()

  console.log(num)
  ```

-  获取可视窗口宽度
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const num = CommonUtils.getPageViewWidth()

  console.log(num)
  ```

-  打开浏览器全屏
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  CommonUtils.toFullScreen()
  ```

-  退出浏览器全屏
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  CommonUtils.exitFullscreen()
  ```

#### 校验相关工具

-  判断是否为数字
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const flag = CommonUtils.checkNum(1)

  console.log(flag)
  ```

-  判断是否为字母
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const flag = CommonUtils.checkLetter('a')

  console.log(flag)
  ```

-  判断是否全部是小写字母
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const flag = CommonUtils.checkLowercaseLetter('avaD')

  console.log(flag)
  ```

-  判断是否是大写字母
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const flag = CommonUtils.checkCapitalLetter('avaD')

  console.log(flag)
  ```

-  判断是否是字母或数字
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const flag = CommonUtils.checkNumOrLetter('avaDada121')

  console.log(flag)
  ```

-  判断是否是中文
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const flag = CommonUtils.checkNumOrLetter('判断是否是中文')

  console.log(flag)
  ```

-  判断是否是中文，数字或字母
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const flag = CommonUtils.checkNumOrLetter('判断是否是中文112da')

  console.log(flag)
  ```

-  判断是否是邮箱地址
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const flag = CommonUtils.checkEmail('12121@qq.com')

  console.log(flag)
  ```

-  判断是否是手机号
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const flag = CommonUtils.checkTelPhone('12134111324')

  console.log(flag)
  ```

-  判断是否是正确的网址
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const flag = CommonUtils.checkUrl('https://www.baidu.com')

  console.log(flag)
  ```

-  校验身份证号码
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const flag = CommonUtils.checkUrl('121212121221')

  console.log(flag)
  ```


-  校验是否为中国大陆的邮政编码
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const flag = CommonUtils.isPostCode(12111)

  console.log(flag)
  ```

-  是否有emoji表情
  
  ```typescript
  import CommonUtils from 'forspace-common-utils'

  const flag = CommonUtils.isEmojiCharacter('👌')

  console.log(flag)
  ```