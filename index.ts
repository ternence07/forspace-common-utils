/*
 * @description: 
 * @Author: T.W
 * @Github: https://github.com/TernenceWu0702
 * @Date: 2021-07-23 10:01:19
 * @LastEditors: T.W
 * @LastEditTime: 2021-11-01 17:52:01
 */
import * as ArrUtils from './lib/arr';
import * as CheckUtils from './lib/check';
import * as ClientUtils from './lib/client';
import * as ObjectUtils from './lib/obj';
import * as StorageUtils from './lib/storage';
import * as StringUtils from './lib/str';
import * as JsUtils from './lib/js';
import * as TimeUtils from './lib/time';
import * as EventUtils from './lib/event';
import * as NumberUtils from './lib/number'
import * as BrowserUtils from './lib/browser'

export default {
    ...BrowserUtils,
    ...NumberUtils,
    ...ArrUtils,
    ...CheckUtils,
    ...ClientUtils,
    ...StorageUtils,
    ...ObjectUtils,
    ...StringUtils,
    ...JsUtils,
    ...TimeUtils,
    ...EventUtils
}