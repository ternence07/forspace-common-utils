/*
 * @description: 
 * @Author: T.W
 * @Github: https://github.com/TernenceWu0702
 * @Date: 2021-07-23 14:08:36
 * @LastEditors: T.W
 * @LastEditTime: 2021-11-01 16:53:47
 */

/**
 * @description: 去掉字符左右空格
 * @param {string} str 字符串
 * @return {*} 字符串
 */
export const strTrimLeftOrRight = (str: string) => {
    return str.replace(/(^\s*)|(\s*$)/g, "")
}

/**
 * @description: 全局替换某个字符为另一个字符
 * @param {string} str 字符
 * @param {string} valueOne 包含的字符
 * @param {string} valueTwo 要替换的字符,选填
 * @return {*} 字符
 */
export const strReplace = (str: string, valueOne: string, valueTwo: string = "") => {
    return str.replace(new RegExp(valueOne, 'g'), valueTwo)
}

/**
 * @description: 将字母全部转化成以大写开头
 * @param {string} str 字符
 * @return {*} 字符
 */
export const strToCapitalLetter = (str: string) => {
    const strOne: string = str.toLowerCase()
    return strOne.charAt(0).toUpperCase() + strOne.slice(1)
}

/**
 * @description: 生成随机字符串
 * @param {number} len
 * @return {*}
 */
export const randomString = (len: number): string => {
    let chars: string = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz123456789';
    let strLen: number = chars.length;
    let randomStr: string = '';
    for (let i: number = 0; i < len; i++) {
        randomStr += chars.charAt(Math.floor(Math.random() * strLen));
    }
    return randomStr;
};

/**
 * @description: 字符串首字母大写
 * @param {string} str
 * @return {*}
 */
export const fistLetterUpper = (str: string): string => {
    return str.charAt(0).toUpperCase() + str.slice(1);
};

/**
 * @description: 手机号中间四位变成*
 * @param {string} tel
 * @return {*}
 */
export const telFormat = (tel: string): string => {
    tel = String(tel);
    return tel.substr(0, 3) + "****" + tel.substr(7);
};

/**
 * @description: 驼峰命名转换成短横线命名
 * @param {string} str
 * @return {*}
 */
export const getKebabCase = (str: string): string => {
    return str.replace(/[A-Z]/g, (item) => '-' + item.toLowerCase())
}

/**
 * @description: 短横线命名转换成驼峰命名
 * @param {string} str
 * @return {*}
 */
export const getCamelCase = (str: string): string => {
    return str.replace(/-([a-z])/g, (i, item) => item.toUpperCase())
}

/**
 * @description: 全角转换为半角
 * @param {string} str
 * @return {*}
 */
export const toCDB = (str: string): string => {
    let result: string = "";
    for (let i = 0; i < str.length; i++) {
        const code: number = str.charCodeAt(i);
        if (code >= 65281 && code <= 65374) {
            result += String.fromCharCode(str.charCodeAt(i) - 65248);
        } else if (code == 12288) {
            result += String.fromCharCode(str.charCodeAt(i) - 12288 + 32);
        } else {
            result += str.charAt(i);
        }
    }
    return result;
}

/**
 * @description: 半角转换为全角
 * @param {string} str
 * @return {*}
 */
export const toDBC = (str: string): string => {
    let result: string = "";
    for (let i = 0; i < str.length; i++) {
        const code: number = str.charCodeAt(i);
        if (code >= 33 && code <= 126) {
            result += String.fromCharCode(str.charCodeAt(i) + 65248);
        } else if (code == 32) {
            result += String.fromCharCode(str.charCodeAt(i) + 12288 - 32);
        } else {
            result += str.charAt(i);
        }
    }
    return result;
}