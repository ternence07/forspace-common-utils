/*
 * @description: CHECK
 * @Author: T.W
 * @Github: https://github.com/TernenceWu0702
 * @Date: 2021-07-23 10:01:41
 * @LastEditors: T.W
 * @LastEditTime: 2021-11-02 11:50:59
 */

/**
 * 判断是否为数字
 * @param val 判断对象
 * @returns boolean
 */
export const checkNum = (val: any) => {
    const reg = /^\d{1,}$/g
    return reg.test(val)
}

/**
 * 判断是否为字母
 * @param val 判断对象
 * @returns boolean
 */
export const checkLetter = (val: any) => {
    const reg = /^[a-zA-Z]+$/g
    return reg.test(val)
}

/**
 * 判断是否全部是小写字母
 * @param val 判断对象
 * @returns boolean
 */
export const checkLowercaseLetter = (val: any) => {
    const reg = /^[a-z]+$/g
    return reg.test(val)
}

/**
 * 判断是否是大写字母
 * @param val 判断对象
 * @returns boolean
 */
export const checkCapitalLetter = (val: any) => {
    const reg = /^[A-Z]+$/g
    return reg.test(val)
}

/**
 * 判断是否是字母或数字
 * @param val 判断对象
 * @returns boolean
 */
export const checkNumOrLetter = (val: any) => {
    const reg = /^[0-9a-zA-Z]*$/g
    return reg.test(val)
}

/**
 * 判断是否是中文
 * @param val 判断对象
 * @returns boolean
 */
export const checkChinese = (val: any) => {
    const reg = /^[\u4E00-\u9FA5]+$/g
    return reg.test(val)
}

/**
 * 判断是否是中文，数字或字母
 * @param val 判断对象
 * @returns boolean
 */
export const checkChineseNumberLetter = (val: any) => {
    const reg = /^[a-zA-Z0-9\u4e00-\u9fa5]+$/g
    return reg.test(val)
}

/**
 * 判断是否是邮箱地址
 * @param val 判断对象
 * @returns boolean
 */
export const checkEmail = (val: any) => {
    const reg = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/g
    return reg.test(val)
}

/**
 * 判断是否是手机号，只要是13,14,15,16,17,18,19开头即可
 * @param val 判断对象
 * @returns boolean
 */
export const checkTelPhone = (val: any) => {
    const reg = /^((\+|00)86)?1[3-9]\d{9}$/g
    return reg.test(val)
}

/**
 * 判断是否是正确的网址
 * @param val 判断对象
 * @returns boolean
 */
export const checkUrl = (val: any) => {
    const a = document.createElement('a')
    a.href = val
    return [
        /^(http|https):$/.test(a.protocol),
        a.host,
        a.pathname !== val,
        a.pathname !== `/${val}`
    ].find(x => !x) === undefined
}

/**
 * @description: 校验身份证号码
 * @param {string} value
 * @return {*}
 */
export const checkCardNo = (value: string): boolean => {
    let reg = /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/;
    return reg.test(value);
};

/**
 * @description: 校验是否为中国大陆的邮政编码
 * @param {number} value
 * @return {*}
 */
export const isPostCode = (value: number): boolean => {
    return /^[1-9][0-9]{5}$/.test(value.toString());
}


/**
 * @description: 是否有emoji表情
 * @param {string} value
 * @return {*}
 */
export const isEmojiCharacter = (value: string): boolean => {
    value = String(value);
    for (let i = 0; i < value.length; i++) {
        const hs = value.charCodeAt(i);
        if (0xd800 <= hs && hs <= 0xdbff) {
            if (value.length > 1) {
                const ls = value.charCodeAt(i + 1);
                const uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                if (0x1d000 <= uc && uc <= 0x1f77f) {
                    return true;
                }
            }
        } else if (value.length > 1) {
            const ls = value.charCodeAt(i + 1);
            if (ls == 0x20e3) {
                return true;
            }
        } else {
            if (0x2100 <= hs && hs <= 0x27ff) {
                return true;
            } else if (0x2B05 <= hs && hs <= 0x2b07) {
                return true;
            } else if (0x2934 <= hs && hs <= 0x2935) {
                return true;
            } else if (0x3297 <= hs && hs <= 0x3299) {
                return true;
            } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030
                || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b
                || hs == 0x2b50) {
                return true;
            }
        }
    }
    return false;
}