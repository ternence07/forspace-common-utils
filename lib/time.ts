/*
 * @description: 
 * @Author: T.W
 * @Github: https://github.com/TernenceWu0702
 * @Date: 2021-07-23 15:00:08
 * @LastEditors: T.W
 * @LastEditTime: 2021-07-23 15:59:43
 */

/**
 * @description: 获取年份
 * @param {Date} date 日期
 * @return {*} 年份
 */
export const getYear = (date: Date = new Date()) => {
    return date.getFullYear()
}

/**
 * @description: 获取当前月份
 * @param {boolean} fillFlag 是否补 0,默认为 true
 * @param {Date} date 日期
 * @return {*} 月份
 */
export const getMonth = (date: Date = new Date(), fillFlag: boolean = true) => {
    const mon = date.getMonth() + 1
    const monRe = mon
    if (fillFlag) mon < 10 ? `0${mon}` : mon
    return monRe
}

/**
 * @description: 获取日
 * @param {boolean} fillFlag 是否补 0
 * @param {Date} date 日期
 * @return {*} 日
 */
export const getDay = (date: Date = new Date(), fillFlag: boolean = true) => {
    const day = date.getDate()
    const dayRe = day
    if (fillFlag) day < 10 ? `0${day}` : day
    return dayRe
}

/**
 * @description: 获取星期几
 * @param {Date} date 日期
 * @return {*} 星期几
 */
export const getWhatDay = (date: Date = new Date()) => {
    return date.getDay() ? new Date().getDay() : 7
}

/**
 * @description: 获取当前月天数
 * @param {number} year 年份
 * @param {number} month 月份
 * @return {*} 天数
 */
export const getMonthNum = (year: number, month: number) => {
    var d = new Date(year, month, 0)
    return d.getDate()
}

/**
 * @description: 获取当前时间
 * @param {Date} date 日期
 * @return {*} 时间 yyyy-mm-dd hh:mm:ss
 */
export const getYyMmDdHhMmSs = (date: Date = new Date()) => {
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    const hours = date.getHours()
    const mi = date.getMinutes()
    const second = date.getSeconds()
    const arr = [month, day, hours, mi, second]
    arr.forEach(item => {
        item < 10 ? '0' + item : item
    })
    return `${year}-${arr[0]}-${arr[1]} ${arr[2]}:${arr[3]}:${arr[4]}`
}

const getZF = (time: number) => {
    return +time < 10 ? `0${time}` : time
}

/**
 * @description: 时间戳转化为年月日
 * @param {any} times 时间戳
 * @param {string} ymd 格式类型(yyyy-mm-dd,yyyy/mm/dd)
 * @param {string} hms 可选,格式类型(hh,hh:mm,hh:mm:ss)
 * @return {*} 年月日
 */
export const timesToYyMmDd = (times: any, ymd: string, hms?: string) => {
    const oDate = new Date(times)
    const oYear = oDate.getFullYear()
    const oMonth = oDate.getMonth() + 1
    const oDay = oDate.getDate()
    const oHour = oDate.getHours()
    const oMin = oDate.getMinutes()
    const oSec = oDate.getSeconds()
    let oTime // 最后拼接时间
    // 年月日格式
    switch (ymd) {
        case 'yyyy-mm-dd':
            oTime = oYear + '-' + getZF(oMonth) + '-' + getZF(oDay)
            break
        case 'yyyy/mm/dd':
            oTime = oYear + '/' + getZF(oMonth) + '/' + getZF(oDay)
            break
    }
    // 时分秒格式
    switch (hms) {
        case 'hh':
            oTime = ' ' + oTime + getZF(oHour)
            break
        case 'hh:mm':
            oTime = oTime + getZF(oHour) + ':' + getZF(oMin)
            break
        case 'hh:mm:ss':
            oTime = oTime + getZF(oHour) + ':' + getZF(oMin) + ':' + getZF(oSec)
            break
    }
    return oTime
}

/**
 * @description: 将年月日转化成时间戳
 * @param {string} time time yyyy/mm/dd 或yyyy-mm-dd 或yyyy-mm-dd hh:mm 或yyyy-mm-dd hh:mm:ss
 * @return {*} 时间戳
 */
export const YyMmDdToTimes = (time: string) => {
    return new Date(time.replace(/-/g, '/')).getTime()
}
