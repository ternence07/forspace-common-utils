/*
 * @description: 浏览器相关操作
 * @Author: T.W
 * @Github: https://github.com/TernenceWu0702
 * @Date: 2021-11-01 17:44:06
 * @LastEditors: T.W
 * @LastEditTime: 2021-11-01 17:51:25
 */

/**
 * @description: 滚动到页面顶部
 * @param {*}
 * @return {*}
 */
export const scrollToTop = () => {
    const height = document.documentElement.scrollTop || document.body.scrollTop;
    if (height > 0) {
        window.requestAnimationFrame(scrollToTop);
        window.scrollTo(0, height - height / 8);
    }
}

/**
 * @description: 滚动到页面底部
 * @param {*}
 * @return {*}
 */
export const scrollToBottom = () => {
    window.scrollTo(0, document.documentElement.clientHeight);
}

/**
 * @description: 滚动到指定元素区域
 * @param {string} element
 * @return {*}
 */
export const smoothScroll = (element: string) => {
    document.querySelector(element)?.scrollIntoView({
        behavior: 'smooth'
    });
};

/**
 * @description: 获取可视窗口高度
 * @return {*}
 */
export const getClientHeight = (): number => {
    let clientHeight: number = 0;
    if (document.body.clientHeight && document.documentElement.clientHeight) {
        clientHeight = (document.body.clientHeight < document.documentElement.clientHeight) ? document.body.clientHeight : document.documentElement.clientHeight;
    }
    else {
        clientHeight = (document.body.clientHeight > document.documentElement.clientHeight) ? document.body.clientHeight : document.documentElement.clientHeight;
    }
    return clientHeight;
}

/**
 * @description: 获取可视窗口宽度
 * @return {*}
 */
export const getPageViewWidth = (): number => {
    return (document.compatMode == "BackCompat" ? document.body : document.documentElement).clientWidth;
}

/**
 * @description: 打开浏览器全屏
 * @param {*}
 * @return {*}
 */
export const toFullScreen = () => {
    let element = document.body;
    if (element.requestFullscreen) {
        element.requestFullscreen()
    }
}

/**
 * @description: 退出浏览器全屏
 * @param {*}
 * @return {*}
 */
export const exitFullscreen = () => {
    if (document.exitFullscreen) {
        document.exitFullscreen()
    }
}