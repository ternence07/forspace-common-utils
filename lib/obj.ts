/*
 * @description: 对象工具
 * @Author: T.W
 * @Github: https://github.com/TernenceWu0702
 * @Date: 2021-07-23 10:45:55
 * @LastEditors: T.W
 * @LastEditTime: 2021-07-23 13:44:00
 */

/**
 * 空对象判断
 * @param obj 对象
 * @returns boolean
 */
export const isEmptyObject = (obj: object) => {
    if (!obj || typeof obj !== 'object' || Array.isArray(obj))
        return false
    return !Object.keys(obj).length
}

/**
 * 判断两个对象是否相等
 * @param oneObj 对象
 * @param twoObj 对象
 * @returns boolean
 */
export const objIsEqual = (oneObj: object, twoObj: object) => {
    var aProps = Object.getOwnPropertyNames(oneObj);
    var bProps = Object.getOwnPropertyNames(twoObj);
    if (aProps.length != bProps.length) {
        return false;
    }
    for (var i = 0; i < aProps.length; i++) {
        var propName = aProps[i]

        var propA = oneObj[propName]
        var propB = twoObj[propName]
        if ((typeof (propA) === 'object')) {
            if (objIsEqual(propA, propB)) {
                return true
            } else {
                return false
            }
        } else if (propA !== propB) {
            return false
        } else { }
    }
    return true
}


export const deepClone = (obj: object) => {
    // 判断拷贝的要进行深拷贝的是数组还是对象，是数组的话进行数组拷贝，对象的话进行对象拷贝
    var objClone = Array.isArray(obj) ? [] : {};
    //进行深拷贝的不能为空，并且是对象或者是
    if (obj && typeof obj === "object") {
        for (let key in obj) {
            if (obj.hasOwnProperty(key)) {
                if (obj[key] && typeof obj[key] === "object") {
                    objClone[key] = deepClone(obj[key]);
                } else {
                    objClone[key] = obj[key];
                }
            }
        }
    }
    return objClone;
}

