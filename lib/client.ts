/*
 * @description: 客户端工具类
 * @Author: T.W
 * @Github: https://github.com/TernenceWu0702
 * @Date: 2021-07-23 10:19:25
 * @LastEditors: T.W
 * @LastEditTime: 2021-11-01 17:43:03
 */

/**
 * 判断浏览器内核
 * @returns 浏览器内核
 */
export const checkBrowser = () => {
    const u = navigator.userAgent;
    const obj = {
        trident: u.indexOf("Trident") > -1, //IE内核
        presto: u.indexOf("Presto") > -1, //opera内核
        webKit: u.indexOf("AppleWebKit") > -1, //苹果、谷歌内核
        gecko: u.indexOf("Gecko") > -1 && u.indexOf("KHTML") == -1, //火狐内核
    }
    return Object.keys(obj)[Object.values(obj).indexOf(true)]
};

/**
 * 判断是终端类型,值有ios,android,iPad
 * @returns 终端类型
 */
export const checkIosAndroidIPad = () => {
    const u = navigator.userAgent;
    const obj = {
        ios: !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/), //ios终端
        android: u.indexOf("Android") > -1 || u.indexOf("Linux") > -1, //android终端或者uc浏览器
        iPad: u.indexOf("iPad") > -1, //是否iPad
    }
    return Object.keys(obj)[Object.values(obj).indexOf(true)]
};

/**
 * 判断是否是微信,qq 或 uc
 * @returns 微信,qq 或 uc
 */
export const checkWeixinQqUc = () => {
    const u = navigator.userAgent;
    const obj = {
        weixin: u.indexOf("MicroMessenger") > -1, //是否微信
        qq: u.match(/QQ/i) && !(u.indexOf('MQQBrowser') > -1), //是否QQ
        uc: u.indexOf('UCBrowser') > -1
    }
    return Object.keys(obj)[Object.values(obj).indexOf(true)]
};

/**
 * 检查是否是 iPhoneX
 * @returns boolean
 */
export const checkIsIPhoneX = () => {
    const u = navigator.userAgent;
    const isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
    if (isIOS && screen.height >= 812) {
        return true;
    }
    return false
};

/**
 * @description: 判断是移动还是PC设备
 * @param {*}
 * @return {*}
 */
export const isMobile = () => {
    if ((navigator.userAgent.match(/(iPhone|iPod|Android|ios|iOS|iPad|Backerry|WebOS|Symbian|Windows Phone|Phone)/i))) {
        return 'mobile';
    }
    return 'desktop';
}

/**
 * @description: 判断是否是苹果还是安卓移动设备
 * @param {*}
 * @return {*}
 */
export const isAppleMobileDevice = () => {
    let reg = /iphone|ipod|ipad|Macintosh/i;
    return reg.test(navigator.userAgent.toLowerCase());
}

/**
 * @description: 判断是否是安卓移动设备
 * @param {*}
 * @return {*}
 */
export const isAndroidMobileDevice = () => {
    return /android/i.test(navigator.userAgent.toLowerCase());
}

/**
 * @description: 判断是Windows还是Mac系统
 * @param {*}
 * @return {*}
 */
export const osType = () => {
    const agent = navigator.userAgent.toLowerCase();
    const isMac = /macintosh|mac os x/i.test(navigator.userAgent);
    const isWindows = agent.indexOf("win64") >= 0 || agent.indexOf("wow64") >= 0 || agent.indexOf("win32") >= 0 || agent.indexOf("wow32") >= 0;
    if (isWindows) {
        return "windows";
    }
    if (isMac) {
        return "mac";
    }
}


/**
 * @description: 浏览器型号和版本
 * @param {*}
 * @return {*}
 */
export const getExplorerInfo = () => {
    let t: string = navigator.userAgent.toLowerCase() || "";
    return 0 <= t.indexOf("msie") ? { //ie < 11
        type: "IE",
        version: Number((t.match(/msie ([\d]+)/) || [])[1])
    } : !!t.match(/trident\/.+?rv:(([\d.]+))/) ? { // ie 11
        type: "IE",
        version: 11
    } : 0 <= t.indexOf("edge") ? {
        type: "Edge",
        version: Number((t.match(/edge\/([\d]+)/) || [])[1])
    } : 0 <= t.indexOf("firefox") ? {
        type: "Firefox",
        version: Number((t.match(/firefox\/([\d]+)/) || [])[1])
    } : 0 <= t.indexOf("chrome") ? {
        type: "Chrome",
        version: Number((t.match(/chrome\/([\d]+)/) || [])[1])
    } : 0 <= t.indexOf("opera") ? {
        type: "Opera",
        version: Number((t.match(/opera.([\d]+)/) || [])[1])
    } : 0 <= t.indexOf("Safari") ? {
        type: "Safari",
        version: Number((t.match(/version\/([\d]+)/) || [])[1])
    } : {
        type: t,
        version: -1
    }
}