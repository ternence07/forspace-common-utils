
/*
 * @description: 数字相关操作
 * @Author: T.W
 * @Github: https://github.com/TernenceWu0702
 * @Date: 2021-11-01 16:23:17
 * @LastEditors: T.W
 * @LastEditTime: 2021-11-01 16:55:52
 */

/**
 * 生成指定范围随机数
 * @param min number 最小值
 * @param max number 最大值
 * @returns number
 */
export const randomNum = (min: number, max: number) => {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 * 数字千分位分隔
 * @param n 需要转换的数字
 * @returns string
 */
export const format = (n: number) => {
    let num: string = n.toString();
    let len: number = num.length;
    if (len <= 3) {
        return num;
    } else {
        let temp: string = '';
        let remainder: number = len % 3;
        if (remainder > 0) { // 不是3的整数倍
            return num.slice(0, remainder) + ',' + num.slice(remainder, len).match(/\d{3}/g)?.join(',') + temp;
        } else { // 3的整数倍
            return num.slice(0, len).match(/\d{3}/g)?.join(',') + temp;
        }
    }
}

/**
 * @description: 数字转化为大写金额
 * @param {number} n
 * @return {*}
 */
const digitUppercase = (n: number): string => {
    const fraction: string[] = ['角', '分'];
    const digit: string[] = [
        '零', '壹', '贰', '叁', '肆',
        '伍', '陆', '柒', '捌', '玖'
    ];
    const unit: string[][] = [
        ['元', '万', '亿'],
        ['', '拾', '佰', '仟']
    ];
    n = Math.abs(n);
    let s: string = '';
    for (let i = 0; i < fraction.length; i++) {
        s += (digit[Math.floor(n * 10 * Math.pow(10, i)) % 10] + fraction[i]).replace(/零./, '');
    }
    s = s || '整';
    n = Math.floor(n);
    for (let i = 0; i < unit[0].length && n > 0; i++) {
        let p = '';
        for (let j = 0; j < unit[1].length && n > 0; j++) {
            p = digit[n % 10] + unit[1][j] + p;
            n = Math.floor(n / 10);
        }
        s = p.replace(/(零.)*零$/, '').replace(/^$/, '零') + unit[0][i] + s;
    }
    return s.replace(/(零.)*零元/, '元')
        .replace(/(零.)+/g, '零')
        .replace(/^整$/, '零元整');
};

/**
 * @description: 数字转化为中文数字
 * @param {number} value
 * @return {*}
 */
const intToChinese = (value: number): string => {
    const str: string = String(value);
    const len: number = str.length - 1;
    const idxs: string[] = ['', '十', '百', '千', '万', '十', '百', '千', '亿', '十', '百', '千', '万', '十', '百', '千', '亿'];
    const num: string[] = ['零', '一', '二', '三', '四', '五', '六', '七', '八', '九'];
    return str.replace(/([1-9]|0+)/g, ($, $1, idx, full) => {
        let pos: number = 0;
        if ($1[0] !== '0') {
            pos = len - idx;
            if (idx == 0 && $1[0] == 1 && idxs[len - idx] == '十') {
                return idxs[len - idx];
            }
            return num[$1[0]] + idxs[len - idx];
        } else {
            let left: number = len - idx;
            let right: number = len - idx + $1.length;
            if (Math.floor(right / 4) - Math.floor(left / 4) > 0) {
                pos = left - left % 4;
            }
            if (pos) {
                return idxs[pos] + num[$1[0]];
            } else if (idx + $1.length >= len) {
                return '';
            } else {
                return num[$1[0]]
            }
        }
    });
}
